﻿using System.Collections.Generic;
using System.Reactive.Linq;

namespace RechManch
{
    public class TotalRec: List<string>
    {
        public int TOTALCOUNT { get; set; }

        public TotalRec()
        {
        }
        
        public List<string> ExpItems
        {
            get
            {
                return this;
            }
        }

        public override string ToString()
        {
            return "Es konnten "+ TOTALCOUNT + " Buchungen eingelesen werden";
        }
    }
}
