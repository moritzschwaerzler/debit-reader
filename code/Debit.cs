﻿using System;

namespace RechManch
{
    public class Debit
    {
        public Debit(DateTime datum, int bid, decimal refnr, decimal betrag)
        {
            Date = datum;
            BankID = bid;
            RefNr = refnr;
            Betrag = betrag;
        }
        public DateTime Date { get; set; }
        public int BankID { get; set; }
        public decimal RefNr { get; set; }
        public decimal Betrag { get; set; }

        public override string ToString()
        {
            return $"Datum: {Date.ToShortDateString()},  BankID: {BankID},  Referenznummer: {RefNr},  Betrag: {Betrag:0.00}";
        }
    }

}
