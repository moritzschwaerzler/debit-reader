﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RechManch
{
    /// <summary>
    /// Interaktionslogik für MsgBoxAutoClose.xaml
    /// </summary>
    public partial class MsgBoxAutoClose : Window
    {
        int count = 0, milisek;
        string title, content;
        public MsgBoxAutoClose(string texttitle, string textcontent, int milis)
        {
            content = textcontent;
            title = texttitle;
            milisek = milis;
            InitializeComponent();
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(new Action(() =>
            {
               Close();
            }), null);
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Timer t = new Timer();
            t.Interval = milisek;
            t.Elapsed += new ElapsedEventHandler(t_Elapsed);
            t.Start();
            MessageBox.Show(content, title);
        }
    }
}
