﻿using System;
using System.IO;
using System.Xml;

namespace RechManch
{

    //IEnumerator<Debit> test123;

    public enum DebitReaderState
        {
            Error,
            Active,
            Finished
        }

    public class DebitReader //: IDebitReader
    {
        #region fields
        string fileName;
        bool booldebittype;
        const int DataSize = 128, TotalSize = 87;
        int Size;
        byte[] data = new byte[129];
        Stream stream;
        XmlReader xmlreader;
        DebitReaderState readerstate;
        bool testprfsum;
        Debit d;
        #endregion

        public DebitReader(string FileName, string debittype, bool TestPrfSum)
        {
            fileName = FileName;
            booldebittype = ReadDebitType(debittype);
             testprfsum = TestPrfSum;
        }

        public DebitReaderState GetState()
        {
            return readerstate;
        }

        public Debit GetCurrentDebit()
        {
            return d;
        }

        public DebitReaderState Read()
        {
            try
            {
                decimal refnr = 0;
                DateTime dt = new DateTime();
                decimal betr = 0;
                int bid = 0;

                #region V11
                if (booldebittype)
                {
                    if (stream == null) 
                    {
                        stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                        readerstate = DebitReaderState.Active;
                    }

                    Size = stream.Read(data, 1, DataSize);

                    if (Size == DataSize && data[100] == 48)// || Size == TotalSize)
                    {
                        //Betrag
                        int zahl = 0;

                        for (int i = 40; i <= 49; i++)
                        {
                            zahl = zahl * 10 + ReadByteToInt(data[i]);
                        }
                        betr = zahl;

                        betr /= 100;

                        //Refnr
                        for (int i = 19; i <= 38; i++)
                        {
                            refnr = refnr * 10 + ReadByteToInt(data[i]);
                        }

                        byte[] refArray = new byte[26];
                        int a = 0;
                        byte actualbyte = 0;

                        for (int i = 13; i <= 38; i++)
                        {
                            actualbyte = data[i];
                            refArray[a] = actualbyte;
                            a++;
                        }

                        //BankID
                        for (int i = 12; i <= 17; i++)
                        {
                            bid = bid * 10 + ReadByteToInt(data[i]);
                        }

                        //Date
                        int year = 2000 + 10 * ReadByteToInt(data[60]);
                        year += ReadByteToInt(data[61]);
                        int month = 10 * ReadByteToInt(data[62]);
                        month += ReadByteToInt(data[63]);
                        int day = 10 * ReadByteToInt(data[64]);
                        day += ReadByteToInt(data[65]);

                        dt = new DateTime(year, month, day);

                        if (testprfsum)
                        {
                            if (data[39] == PrfSumCalc.ReturnPrfSumByte(refArray))
                            {
                                d = new Debit(dt, bid, refnr, betr);
                            }
                            else
                            {
                                throw new Exception("Prüfsumme falsch");
                            }
                        }
                        else
                        {
                            d = new Debit(dt, bid, refnr, betr);

                        }

                          readerstate = DebitReaderState.Active;
                          return readerstate;
                    }
                    else if(Size == TotalSize && data[86] == 48)
                    {
                        readerstate = DebitReaderState.Finished;
                        stream.Close();
                        stream = null;
                        return readerstate;
                    }
                    else
                    {
                        readerstate = DebitReaderState.Error;
                        stream.Close();
                        stream = null;
                        return readerstate;
                    }
                }
                #endregion
                #region XML
                else
                {
                    if (xmlreader == null)  
                    {
                        xmlreader = XmlReader.Create(fileName);
                        if (xmlreader.ReadToFollowing("Document"))
                        {
                            string typ = xmlreader.GetAttribute("xmlns");
                            if (typ == "")
                            {
                                throw new Exception("Flasches Format nur vollständige camt.054*.xml Dateien einlesen!");
                            } 
                            else if (typ.Contains("camt.054"))
                            {
                                readerstate = DebitReaderState.Active;
                            }
                            else
                            {
                                throw new Exception("Flasches Format nur vollständige camt.054*.xml Dateien einlesen!");
                            }
                        }
                        else
                        {
                            readerstate = DebitReaderState.Error;
                            return readerstate;
                        }
                    }
                    else if (xmlreader.ReadState != ReadState.Interactive)
                    {
                        throw new Exception("XMLReadstate: " + xmlreader.ReadState);
                    }

                    if (xmlreader.ReadToFollowing("TxDtls"))
                    { 
                        //Betrag
                        betr = ReadXMLElementDecimal("Amt");

                        //Refnr + BankID
                        char fileprfsum = '\0';
                        string refstring = "";
                        string isrref = "";
                        string baid = "";

                        if (!xmlreader.ReadToFollowing("RmtInf")) throw new Exception("Konnte Referenznummer nicht lesen");

                        if (ReadXMLElementString("Prtry") == "ISR Reference")
                        {
                            isrref = ReadXMLElementString("Ref");
                        }
                        else
                        {
                            throw new Exception("Keine ISR Referenznummer");
                        }

                        for (int i = 0; i < 7; i++)
                        {
                            baid += isrref[i];
                        }
                        bid = int.Parse(baid);

                        for (int i = 6; i < 26; i++)
                        {
                            refstring += isrref[i];
                        }
                        refnr = decimal.Parse(refstring);

                        fileprfsum = isrref[26];

                        //Date
                        dt = ReadXMLElementDateTime("AccptncDtTm");


                        if (testprfsum)
                        {
                            if (fileprfsum == PrfSumCalc.ReturnPrfSumStringToChar(refstring))
                            {
                                d = new Debit(dt,bid,refnr,betr);
                            }
                            else
                            {
                                throw new Exception("Prüfsumme falsch");
                            }
                        }
                        else
                        {
                            d = new Debit(dt, bid, refnr, betr);
                        }

                        readerstate = DebitReaderState.Active;
                        return readerstate;
                    }
                    else
                    {
                        readerstate = DebitReaderState.Finished;
                        return readerstate;
                    }
                }
                #endregion
            }
            catch (Exception)
            {
                readerstate = DebitReaderState.Error;
                return readerstate;
            }
        }

        private int ReadByteToInt(byte data)
        {
            if (!(data <= 57 && data >= 48))
            {
                throw new Exception("DR: ReadByteToInt: Eingelesenes Byte ist ein falsches Zeichen (keine Ziffer");
            }
            int outint = data - 48;
            return outint;
        }

        private bool ReadDebitType(string debittype)
        {
            if (string.Equals(debittype, ".v11", StringComparison.OrdinalIgnoreCase)) //Wenn XML oder V11 File...
            {
                return true;
            }
            else if (string.Equals(debittype, ".xml", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
            else
            {
                throw new Exception("DR: ReadDebitType: Bitte *.v11 oder camt054*.xml Datei einlesen");
            }
        }
        
        private string ReadXMLElementString(string XMLTag)
        { 
            //Auslesen des<asdf>Wertes</asdf> eines XML-Tags

            if (xmlreader.ReadToFollowing(XMLTag))
            {
                string str = xmlreader.ReadElementContentAsString();
                return str;
            }
            else
            {
                throw new Exception($"ReadXMLElementString: XML-Tag nicht gefunden: {XMLTag}");
            }
        }

        private decimal ReadXMLElementDecimal(string XMLTag)
        {
            if (xmlreader.ReadToFollowing(XMLTag))
            {
                decimal str = xmlreader.ReadElementContentAsDecimal();
                return str;
            }
            else
            {
                throw new Exception($"ReadXMLElement: XML-Tag nicht gefunden: {XMLTag}");
            }
        }

        private DateTime ReadXMLElementDateTime(string XMLTag)
        {
            if (xmlreader.ReadToFollowing(XMLTag))
            {
                DateTime str = xmlreader.ReadElementContentAsDateTime();
                return str;
            }
            else
            {
                throw new Exception($"ReadXMLElement: XML-Tag nicht gefunden: {XMLTag}");
            }
        }
    }
}
