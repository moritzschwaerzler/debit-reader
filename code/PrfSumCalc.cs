﻿using System;
using System.Text;

namespace RechManch
{
    public class PrfSumCalc
    {
        public static byte ReturnPrfSumByte(byte[] data)
        {
            try
            {
                byte prfsum;
                int length = data.Length;

                int[] tabelle = { 0, 9, 4, 6, 8, 2, 7, 1, 3, 5 };
                int uebertrag = 0;

                for (int i = 0; i < length; i++)
                {
                    byte ziffer = data[i];
                    if (!(ziffer > 57 || ziffer < 48))
                    {
                        int intziffer = ziffer - 48;
                        int sum = intziffer + uebertrag;

                        if (sum >= 10) sum -= 10;

                        uebertrag = tabelle[sum];
                    }
                    else
                    {
                        throw new Exception("Es kann nur mit Ziffern eine Prüfsumme gebildet werden");
                    }
                }

                prfsum = Convert.ToByte(10 - uebertrag + 48);

                if (prfsum == 58)
                {
                    return 48;
                }
                else
                {
                    return prfsum;
                }
            }
            catch (Exception exp)
            {

                throw new Exception("PrfSumCalc: " + exp.Message);
            }
        }

        public static char ReturnPrfSumStringToChar(string data)
        {
            return Convert.ToChar(ReturnPrfSumByte(Encoding.ASCII.GetBytes(data)));
        }
    }
}
