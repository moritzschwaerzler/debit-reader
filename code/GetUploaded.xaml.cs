﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace RechManch
{
    /// <summary>
    /// Interaktionslogik für GetUploaded.xaml
    /// </summary>

    public partial class GetUploaded : Window
    {
        bool cb1 = false, cb2 = false, bid1 = false, refnr1 = false, betr1 = false;
        List<Debit> li;
        int count;
        DateTime dt1;
        DateTime dt2;
        decimal betrag;
        decimal RefNr;
        int bankID;

        #region initwindow
        public GetUploaded()
        {
            InitializeComponent();
        }

        private void cbdate1_Checked(object sender, RoutedEventArgs e)
        {
            cb1 = true;
            dp1.IsEnabled = true;
            cobodt1.IsEnabled = true;
        }

        private void cbdate1_Unchecked(object sender, RoutedEventArgs e)
        {
            cb1 = false;
            dp1.IsEnabled = false;
            cobodt1.IsEnabled = false;
        }

        private void cbdate2_Checked(object sender, RoutedEventArgs e)
        {
            cb2 = true;
            dp2.IsEnabled = true;
            cobodt2.IsEnabled = true;
        }

        private void cbdate2_Unchecked(object sender, RoutedEventArgs e)
        {
            cb2 = false;
            dp2.IsEnabled = false;
            cobodt2.IsEnabled = false;
        }

        private void btabbrechen_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cbbid_Unchecked(object sender, RoutedEventArgs e)
        {
            bid1 = false;
            tbbankid.IsEnabled = false;
        }

        private void cbrefnr_Unchecked(object sender, RoutedEventArgs e)
        {
            refnr1 = false;
            tbrefnr.IsEnabled = false;
        }

        private void cbbetr_Checked(object sender, RoutedEventArgs e)
        {
            betr1 = true;
            tbbetr.IsEnabled = true;
            cobobetr.IsEnabled = true;
        }

        private void cbbetr_Unchecked(object sender, RoutedEventArgs e)
        {
            betr1 = false;
            tbbetr.IsEnabled = false;
            cobobetr.IsEnabled = false;
        }

        private void cbrefnr_Checked(object sender, RoutedEventArgs e)
        {
            refnr1 = true;
            tbrefnr.IsEnabled = true;
        }

        private void cbbid_Checked(object sender, RoutedEventArgs e)
        {
            bid1 = true;
            tbbankid.IsEnabled = true;
        }
        #endregion


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            count = 0;
            li = new List<Debit>();
            try
            {
                using (MySqlConnection con = new MySqlConnection(@"server=127.0.0.1; database=buchung; Uid=root;"))// 
                {
                    con.Open();

                    using (var trans = con.BeginTransaction())
                    {
                        using (var cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trans;

                            cmd.CommandText = CommandStringBulider(cb1, cb2, bid1, refnr1, betr1);

                            #region Command Parameter füllen
                            if (cb1)
                            {
                                var datea = cmd.Parameters.Add("date1", MySqlDbType.Date);
                                datea.Value = dt1;
                            }
                            if (cb2)
                            {
                                var dateb = cmd.Parameters.Add("date2", MySqlDbType.Date);
                                dateb.Value = dt2;
                            }
                            if (bid1)
                            {
                                var bankida = cmd.Parameters.Add("bankID", MySqlDbType.Int32);
                                bankida.Value = bankID;
                            }
                            if (refnr1)
                            {
                                var refnra = cmd.Parameters.Add("refnr", MySqlDbType.Decimal);
                                refnra.Value = RefNr;
                            }
                            if (betr1)
                            {
                                var betraga = cmd.Parameters.Add("betrag", MySqlDbType.Decimal);
                                betraga.Value = betrag;
                            }
                            #endregion

                            #region ergebnisse auslesen
                            var reader = cmd.ExecuteReader();

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DateTime dt = reader.GetDateTime("date");
                                    decimal refnr = reader.GetDecimal("refnr");
                                    decimal betrag = reader.GetDecimal("betrag");
                                    int bankid = reader.GetInt32("bankID");

                                    Debit d = new Debit(dt, bankid, refnr, betrag);
                                    li.Add(d);
                                    count++;
                                }
                            }
                            #endregion
                        }
                    }
                }
                libosel.ItemsSource = null;
                libosel.ItemsSource = li;
                lblout.Content = $"Es wurden {count} Buchungen gefunden";
                MsgBoxAutoClose dlg = new MsgBoxAutoClose("Fertig",$"Es wurden: {count} Buchungen gefunden", 3000);
                dlg.Show();
            }
            catch (Exception exp)
            {
                MessageBox.Show("Es ist ein Fehler aufgetreten: " + exp.Message);
            }
        }

        private string CommandStringBulider(bool booldt1, bool booldt2, bool boolbid, bool boolrefnr, bool boolbetr) 
        {
            string cmdstring = "SELECT * FROM rechnungzwei WHERE ";
            string strrefnr = tbrefnr.Text;
            string strbetrag = tbbetr.Text;
            string strbid = tbbankid.Text;
            int a = 0;

            if (booldt1)
            {
                if (dp1.SelectedDate.HasValue)
                {
                    dt1 = dp1.SelectedDate.Value;
                    a++;
                    cmdstring += "date";
                    switch (cobodt1.SelectedIndex)
                    {
                        case 0:
                            cmdstring += "=";
                            break;
                        case 1:
                            cmdstring += ">=";
                            break;
                        case 2:
                            cmdstring += "<=";
                            break;
                    }
                    cmdstring += "@date1";
                }
                else
                {
                   throw new Exception("Bitte Unteres Datum auswählen");
                }
            }
            if (booldt2)
            {
                if (a>0)
                {
                    cmdstring += " AND ";
                }
                if (dp2.SelectedDate.HasValue)
                {
                    dt2 = dp2.SelectedDate.Value;
                    cmdstring += "date";
                    switch (cobodt2.SelectedIndex)
                    {
                        case 0:
                            cmdstring += "=";
                            break;
                        case 1:
                            cmdstring += ">=";
                            break;
                        case 2:
                            cmdstring += "<=";
                            break;
                    }
                    cmdstring += "@date2";
                    a++;
                }
                else
                {
                    throw new Exception("Bitte Oberes Datum auswählen");
                }
            }
            if (boolbetr)
            {
                if (a>0)
                {
                    cmdstring += " AND ";
                }
                betrag = decimal.Parse(strbetrag);
                cmdstring += "betrag";
                switch (cobobetr.SelectedIndex)
                {
                    case 0:
                        cmdstring += "=";
                        break;
                    case 1:
                        cmdstring += ">=";
                        break;
                    case 2:
                        cmdstring += "<=";
                        break;
                }
                cmdstring += "@betrag";
                a++;
            }
            if (boolrefnr)
            {
                if (a > 0)
                {
                    cmdstring += " AND ";
                }
                if (strrefnr.Length != 20)
                {
                    throw new Exception("Referenznummer muss 20 Stellen haben");
                }
                else
                {
                    RefNr = decimal.Parse(strrefnr);
                    cmdstring += "refnr = @refnr";
                    a++;
                }
            }
            if (boolbid)
            {
                if (a > 0)
                {
                    cmdstring += " AND ";
                }
                if (strbid.Length != 6)
                {
                    throw new Exception("Bank ID  muss 6 Stellen haben");
                }
                else
                {
                    bankID = int.Parse(strbid);
                    cmdstring += "bankid = @bankID";
                    a++;
                }
            }

            if (booldt1&&booldt2)
            {
                if (dt1 > dt2)
                {
                    throw new Exception(@"'Unteres Datum' darf nicht größer als das 'Obere Datum' sein");
                }
            }

            if (a==0)
            {
                return cmdstring + "1";
            }
            else
            {
                return cmdstring;
            }
        }


    }
}
