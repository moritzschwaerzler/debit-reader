﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace RechManch
{
    public class DBWrite
    {
        public static int Write(DebitList list, TotalRec totalrec) 
        {
            int count = 0;

            IEnumerator<Debit> Reader = list.GetEnumerator();

            using (MySqlConnection con = new MySqlConnection(@"server=127.0.0.1; database=buchung; Uid=root;"))// 
            {
                con.Open();

                using (var trans = con.BeginTransaction())
                {
                    using (var cmd = con.CreateCommand())
                    {
                        #region sendecommand
                        cmd.Transaction = trans;

                        cmd.CommandText = "INSERT INTO rechnungzwei (date,bankID,refnr,betrag) VALUES (@date,@bankid,@ref,@betrag)";
                        var date = cmd.Parameters.Add("@date", MySqlDbType.Date);
                        var bid = cmd.Parameters.Add("@bankid", MySqlDbType.Int32);
                        var re = cmd.Parameters.Add("@ref", MySqlDbType.Decimal);
                        var betrag = cmd.Parameters.Add("@betrag", MySqlDbType.Decimal);
                        #endregion

                        int dbcount = 0;

                        while (Reader.MoveNext())
                        {
                            #region aktuelle Werte
                            bool IsNew = false;

                            decimal betr = Reader.Current.Betrag;

                            decimal refnr = Reader.Current.RefNr;

                            int bankide = Reader.Current.BankID;

                            DateTime dt = Reader.Current.Date;
                            #endregion

                            #region IstRechnungNochNichtDrin
                            using (var cmd2 = con.CreateCommand())
                            {
                                cmd2.CommandText = "SELECT * FROM rechnungzwei WHERE date=@datea AND bankID=@bankida AND refnr=@refa AND betrag=@betraga";
                                var datea = cmd2.Parameters.Add("@datea", MySqlDbType.Date);
                                var bida = cmd2.Parameters.Add("@bankida", MySqlDbType.Int32);
                                var rea = cmd2.Parameters.Add("@refa", MySqlDbType.Decimal);
                                var betraga = cmd2.Parameters.Add("@betraga", MySqlDbType.Decimal);

                                betraga.Value = betr;
                                rea.Value = refnr;
                                bida.Value = bankide;
                                datea.Value = dt;

                                var reader = cmd2.ExecuteReader();

                                if (reader.HasRows)
                                {
                                    IsNew = false;
                                }
                                else
                                {
                                    IsNew = true;
                                }
                            }
                            #endregion

                            #region Upload
                            if (IsNew)
                            {
                                betrag.Value = betr;
                                re.Value = refnr;
                                bid.Value = bankide;
                                date.Value = dt;

                                try
                                {
                                    dbcount = cmd.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    totalrec.ExpItems.Add("DBWrite: " + exp.Message);
                                }
                            }
                            else
                            {
                                totalrec.ExpItems.Add("DBWrite: Buchung bereits in Datenbank: " + Reader.Current.ToString());
                            }
                            

                            if (dbcount == 1)
                            {
                                count++;

                                dbcount = 0;
                            }
                            #endregion
                        }
                        trans.Commit();

                        return count;
                    }
                }
            }
        }
    }
}
