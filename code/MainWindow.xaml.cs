﻿using Microsoft.Win32;
using Path = System.IO.Path;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RechManch
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string filename;
        string pathextension;
        public TotalRec totalrec;
        DebitList debitlist;
        int dbcount;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btpfad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                totalrec = new TotalRec();
                filename = openFileDialog.FileName;
                pathextension = Path.GetExtension(filename);
                bool ReadPrfSum;

                if (MessageBoxResult.Yes ==  MessageBox.Show("Sollen Prüfsummen in Referenznummern überprüft werden? \n (wird empfohlen)", "Prüfsummen überprüfen?", MessageBoxButton.YesNo))
                {
                    ReadPrfSum = true;
                }
                else
                {
                    ReadPrfSum = false;
                }

                try
                {
                    debitlist = new DebitList(totalrec, filename, pathextension, ReadPrfSum);

                    if (debitlist.GetCouldRead()) //Ausgabe
                    {
                        GUIAusgabeREAD();
                    }
                    else //Fehler
                    {
                        MessageBox.Show("Beim Einlesen ist ein Fehler aufgetreten. Datei ist nicht lesbar!", "Datei defekt!");
                    }

                }
                catch (Exception exp)
                {
                    MessageBox.Show("MAIN:" + exp.Message);
                }
            }
        }

        private void btupload_Click(object sender, RoutedEventArgs e)
        {
            dbcount = 0;

            try
            {
                dbcount = DBWrite.Write(debitlist, totalrec);
            }
            catch (Exception exp)
            {
                MessageBox.Show("MAIN: " + exp.Message);
            }

            GUIAusgabeWRITE();
        }

        private void btDBSelect_Click(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;
            GetUploaded uploaded = new GetUploaded();
            uploaded.ShowDialog();
            IsEnabled = true;
        }

        private void GUIAusgabeWRITE()
        {
            List<string> li = totalrec.ExpItems;
            liboexp.ItemsSource = null;
            liboexp.ItemsSource = li;
            lblfile.Content += ", davon konnten " + dbcount + " erfolgreich hochgeladen werden.";
            lblexpcount.Content = EXPCountText();
            btupload.IsEnabled = false;
        }
        private void GUIAusgabeREAD()
        {
            lblpfad.Content = filename;
            lblfile.Content = totalrec.ToString();
            

            if (totalrec.TOTALCOUNT > 0)
            {
                libosel.ItemsSource = debitlist;
                libosel.Visibility = Visibility.Visible;

                liboexp.ItemsSource = totalrec.ExpItems;
                liboexp.Visibility = Visibility.Visible;

                btupload.Visibility = Visibility.Visible;
                lblupload.Visibility = Visibility.Visible;
                lblliboexp.Visibility = Visibility.Visible;
                lbllibosel.Visibility = Visibility.Visible;

                lblexpcount.Content = EXPCountText();

                btupload.IsEnabled = true;
            }
        }
        private string EXPCountText()
        {
            return $"Es sind {totalrec.ExpItems.Count} Fehler aufgetreten";
        }
    }
}
