﻿using System.Collections.Generic;
using System.ComponentModel;

namespace RechManch
{
    public class DebitList : List<Debit>
    {
        bool couldread;
        public DebitList( TotalRec totalrec, string FileName, string debittype, bool TestPrfSum)
        {
            DebitReader reader = new DebitReader(FileName, debittype, TestPrfSum);

            while (reader.Read() == DebitReaderState.Active)
            {
                Add(reader.GetCurrentDebit());
                totalrec.TOTALCOUNT++;
            }
            DebitReaderState state = reader.GetState();
            if (state == DebitReaderState.Error)
            {
                totalrec.ExpItems.Add("Fehler in Buchung, Datei nicht lesbar");
                couldread = false;
            }
            else if (state == DebitReaderState.Finished)
            {
                couldread = true;
            }
        }

        public bool GetCouldRead()
        {
            return couldread;
        }
    }
}
