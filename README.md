# Debit Reader

Es soll eine Oberfläche programmiert werden, mithilfe deren Sammelbuchungen (ESR --> z.B. Schweizer Mobil-Banking) eingelesen werden können.

Diese Sammelbuchungen können im camt.054*.xml oder im *.v11 Format sein.

Es sollen aus den Sammelbuchungen die Einzelbuchungen ausgelesen werden.

Diese ausgelesenen Buchungen sollen auf eine Datenbank geschrieben werden.

Jede Einzelbuchung darf nur einmal vorkommen.

Eine Einzelbuchung macht aus:
- Datum
- Betrag
- Referenznummer (20 Stellen), Prüfzimmer kann geprüft werden
- Banknr (6 Stellen)
- (sonstige Daten, welche zu diesem Zwecke nicht benötigt werden)

Auf der GUI sollen nach dem Einlesen bzw. Hochladen die wichtigsten Infos über die Sammelbuchung 
(totalrecord) und das Laden auf DB angezeigt werden.

Weiter sollen die bereits hochgeladenen Buchungen gefiltert angezeigt werden können.


## Startfenster

![image](pics/main-win.png)

## Eingelesene Buchungen

![image](pics/eingelesene.png)


## Bereits hochgeladene Buchungen anzeigen

![image](pics/get-uploaded.png)


